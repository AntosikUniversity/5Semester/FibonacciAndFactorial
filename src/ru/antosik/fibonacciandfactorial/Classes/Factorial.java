package ru.antosik.fibonacciandfactorial.Classes;

/**
 * Class, which implements factorial calculation
 */
public class Factorial {
    /**
     * Calculates factorial of number
     *
     * @param N Number
     * @return Factorial of number
     */
    public static long calculate(long N) {
        if (N < 0) throw new IllegalArgumentException("Number must be greater than 0!");
        long num = 1;
        for (int i = 1; i <= N; i++) {
            num *= i;
        }
        return num;
    }
}
