package ru.antosik.fibonacciandfactorial.Classes;

/**
 * Class, which implements fibonacci algorithm
 */
public class Fibonacci {
    /**
     * Calculates number in sequence position
     *
     * @param N Position in sequence
     * @return Number in position
     */
    public static long calculate(long N) {
        if (N < 1) throw new IllegalArgumentException("Number must be greater than 0!");
        long first = 0;
        long second = 1;
        for (int i = 1; i < N; i++) {
            second = first + second;
            first = second - first;
        }
        return first;
    }
}
