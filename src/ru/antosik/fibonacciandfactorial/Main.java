package ru.antosik.fibonacciandfactorial;

import ru.antosik.fibonacciandfactorial.Classes.Factorial;
import ru.antosik.fibonacciandfactorial.Classes.Fibonacci;

public class Main {

    public static void main(String[] args) {
        System.out.printf("10th fibonacci number - %d %n", Fibonacci.calculate(10));
        System.out.printf("Factorial of 10 - %d %n", Factorial.calculate(10));
    }
}
